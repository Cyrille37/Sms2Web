<?php
/**
 */

error_reporting( E_ALL );
define( 'DEBUG', false );
define( 'INFO', true );

require_once(__DIR__.'/../lib/SmsGw.php');

header('Content-Type', 'application/json');

try
{
	$smsgw = new SmsGw();
	$request = new Request();
	$sms = null ;

	switch( $request->action )
	{
		case 'push':
			if( INFO || DEBUG )
				error_log('push ['.$request->channel.']');
			$smsgw->push( $request->channel, $request->data );
			break;
		case 'pull':
			if( INFO || DEBUG  )
				error_log('pull ['.$request->channel.']');
			$sms = $smsgw->pull( $request->channel, $request->data );
			break;
		default:
			throw new HttpException( 'Unknow action', HttpException::Unprocessable_Entity );
	}
	echo json_encode([
		'status' => 'OK',
		'sms' => $sms
	]);
}
catch( Exception $e )
{
	if( $e instanceof HttpException )
	{
		http_response_code( $e->getCode() );
	}
	else
	{
		http_response_code( HttpException::Internal_Server_Error );
	}
	echo json_encode([
		'status'=>'ERROR',
		'error'=>$e->getMessage(),
		'error_details'=> (DEBUG ? ($e->getFile().' at '.$e->getLine()):'DEBUG OFF')
	]);
}

if( Request::isCLI() )
	echo "\n";

/**
 * The Request abstract running on Web or Command line (Cli).
 * 
 * To run on shell:
 *	- public/gw.php tests/fixture-push-01.json
 *	- public/gw.php tests/fixture-pull-01.json
 */
class Request
{
	public $channel ;
	public $action ;
	public $data ;

	public function __construct()
	{
		global $argv ;

		if( self::isCLI() )
		{
			// For testing
			if(DEBUG)
			{
				error_log('CLI run');
				error_log('Argv: ['.var_export( $argv,true).']');
			}

			$filename = $argv[1];
			if( ! file_exists($filename))
				throw new HttpException( 'File not found ['.$filename.']', HttpException::Unprocessable_Entity );
			$data = @json_decode( file_get_contents($filename) );
			if( ! $data instanceof stdClass )
				throw new HttpException( 'Malformed json', HttpException::Unprocessable_Entity );
			$this->channel = $data->channel ;
			$this->action = $data->action ;
			$this->data = json_encode( $data->data );
		}
		else
		{
			if(DEBUG)
			{
				error_log('HTTP run');
				error_log('Headers: ['.var_export(getallheaders(),true).']');
				error_log('Get: ['.var_export($_GET,true).']');
				error_log('Post: ['.var_export($_POST,true).']');
				$data = file_get_contents('php://input');
				error_log('Content: ['.($data ? $data : 'NULL').']');
			}

			$this->channel = isset($_GET['channel']) ? $_GET['channel'] : null ;
			$this->action = isset($_GET['action']) ? $_GET['action'] : null ;
			if( $this->wantJson() )
			{
				$this->data =  file_get_contents('php://input');
			}
			else
			{
				$this->data = isset($_POST['data']) ? $_POST['data'] : null ;
			}
		}
	}

	protected $headers ;

	public function getHeaders()
	{
		if( $this->headers == null )
			$this->headers = getallheaders();
		return $this->headers ;
	}

	public function getHeader( $key )
	{
		if( $this->headers == null )
			$this->headers = getallheaders();
		return isset($this->headers[$key]) ? $this->headers[$key] : null ;
	}

	public function wantJson()
	{
		// 'Content-Type' => 'application/json; charset=utf-8'
		$h = $this->getHeader('Content-Type') ;
		if( $h == null )
			return false ;
		if( strpos( $h, 'application/json' ) === 0 )
			return true ;
		return false ;
	}

	public static function isCLI()
	{
		// default fpm-fcgi default configuration disable all ENV variables.
		if( php_sapi_name()==='fpm-fcgi' )
			return false ;

		if( empty($_SERVER['REMOTE_ADDR']) && !isset($_SERVER['HTTP_USER_AGENT']) && count($_SERVER['argv']) > 0)
		{
			return true;
		}
		return false;
	}
}
