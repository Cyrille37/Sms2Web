#!/usr/bin/env php
<?php

$channel = 'EE661A2D' ;

$url = 'http://192.168.0.24/smsgw/gw-test.php' ;

// =====================================================
echo '===== PUSH',"\n";

$push_url = $url ;
$push_url.= '?action=push' ;
$push_url.= '&channel='.$channel ;
echo 'URL: [', $push_url,"]","\n";

$opts = array(
  'http'=>array(
    'method' => 'POST',
    'header' => 'Content-Type: application/json'."\r\n",
    'content' => '{"from":"+33632330218","from_at":12313,"to":"+33632330218","to_at":123123,"body": "salut le monde","sign": "f76267c336ccd9e1a89034d285f296541d74de85"}'
  )
);
$context = stream_context_create($opts);
$response = file_get_contents( $push_url, false, $context );
echo 'RESPONSE: [', var_export($response,true),']',"\n";

// =====================================================
echo '===== PULL',"\n";

$pull_url = $url ;
$pull_url.= '?action=pull' ;
$pull_url.= '&channel='.$channel ;
echo 'URL: [', $pull_url,"]","\n";

$opts = array(
  'http'=>array(
    'method' => 'POST',
    'header' => 'Content-Type: application/json'."\r\n",
    'content' => '{"from":"+33632330218","to":"","sign": "704b3726a8fb4640546dafb566e613df611815f5"}'
  )
);
$context = stream_context_create($opts);
$response = file_get_contents( $pull_url, false, $context );
echo 'RESPONSE: [', var_export($response,true),']',"\n";
$response = file_get_contents( $pull_url, false, $context );
echo 'RESPONSE: [', var_export($response,true),']',"\n";
