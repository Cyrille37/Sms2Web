#!/usr/bin/env php
<?php
/**
 * $ ./www/tests/sign.php push FC18CF533AF96071890830D4A8AD7557 www/tests/fixture-push-01.json 
 *	action: push
 *	filename: www/tests/fixture-push-01.json
 *	sign: f76267c336ccd9e1a89034d285f296541d74de85
 * 
 */
$action = $argv[1];
$secret = $argv[2];
$filename = $argv[3];

$data = json_decode( file_get_contents($filename));

switch( $action )
{
	case 'push':
		$msg = $data->data->from . $data->data->from_at . $data->data->to . $data->data->to_at . $data->data->body ;
		break;
	case 'pull':
		$msg = $data->data->from.$data->data->to ;
		break;
	default:
		throw new RuntimeException('Unknow action');
}
$sign = sha1( $secret.$msg );

echo 'action: ',$action,"\n";
echo 'filename: ',$filename,"\n";
echo 'sign: ', $sign, "\n";
