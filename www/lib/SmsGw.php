<?php

class SmsGw
{
	public $config = [
		'db'=>[
			'dsn'=>'mysql:dbname=smsgw;unix_socket=/var/run/mysqld/mysqld.sock',
			'user'=>'root',
			'pass'=>'root',
			'tables' => ['channels'=>'channels','queue'=>'queue']
		]
	];

	public static $table_channel ;
	public static $table_queue ;

	const ACTION_PULL = 1 ;
	const ACTION_PUSH = 2 ;

	/**
	 * @var \Db
	 */
	protected $db ;

	public function __construct()
	{
		$this->db = new Db( $this->config['db']['dsn'], $this->config['db']['user'], $this->config['db']['pass'] );
		self::$table_channel = $this->config['db']['tables']['channels'];
		self::$table_queue = $this->config['db']['tables']['queue'];
	}

	public function push( $channel, $data )
	{
		if(DEBUG)
			error_log(__METHOD__.' channel: ['.$channel.'] decoding json');

		$data = json_decode($data);
		if( ! $data instanceof stdClass )
			throw new HttpException( 'Malformed json', HttpException::Unprocessable_Entity );

		if(DEBUG)
			error_log(__METHOD__.' channel: ['.$channel.'] checking data');

		$channel_id = $this->checkData( self::ACTION_PUSH, $channel, $data );
		if( empty($channel_id) )
			throw new RuntimeException( 'Should not occurs!', HttpException::Internal_Server_Error );

		if(DEBUG)
			error_log(__METHOD__.' channel: ['.$channel.'] updating db');

		unset( $data->sign );
		$data->channel_id = $channel_id ;
		$data->created_at = null ;
		$this->db->insertOrUpdate( self::$table_queue, $data );
	}

	public function pull( $channel, $data )
	{
		if(DEBUG)
			error_log(__METHOD__.' channel: ['.$channel.'] decoding json');

		$data = json_decode($data);
		if( ! $data instanceof stdClass )
			throw new HttpException( 'Malformed json', HttpException::Unprocessable_Entity );

		if(DEBUG)
			error_log(__METHOD__.' channel: ['.$channel.'] checking data');

		$channel_id = $this->checkData( self::ACTION_PULL, $channel, $data );
		if( empty($channel_id) )
			throw new RuntimeException( 'Should not occurs!', HttpException::Internal_Server_Error );

		if(DEBUG)
			error_log(__METHOD__.' channel: ['.$channel.'] querying db for new message');

		$sql = 'SELECT * FROM `'.self::$table_queue.'` WHERE (`channel_id` = ? AND NOT `sent`)';
		$values = [ $channel_id ];
		if( ! empty($data->from) )
		{
			$sql.= ' AND `from` = ?' ;
			array_push( $values, $data->from );
		}
		if( ! empty($data->to) )
		{
			$sql.= ' AND `to` = ?' ;
			array_push( $values, $data->to );
		}

		$data = $this->db->queryOne( $sql, $values );

		if( ! empty($data) )
		{
			if(DEBUG)
				error_log(__METHOD__.' channel: ['.$channel.'] updating db');

			$data->sent = true ;
			$this->db->insertOrUpdate( self::$table_queue, $data );
		}
		return ( $data===false ? null : $data );
	}

	/**
	 * 
	 * @param string $channel The channel identifier
	 * @param object $data StdClass object that contains sms data
	 * @return int The channel id
	 * @throws HttpException
	 */
	protected function checkData( $action, $channel, $data )
	{
		if( ! $data->sign )
			throw new HttpException( 'data not signed', HttpException::Unprocessable_Entity );
		$channel = $this->db->queryOne( 'SELECT * FROM '.self::$table_channel.' WHERE hash=?', [$channel] );
		if( ! $channel )
			throw new HttpException( 'Unknow channel ['.$channel.']', HttpException::Forbidden );

		switch( $action )
		{
			case self::ACTION_PULL:
				if( strtolower($data->sign) != $this->signPull( $channel->secret, $data) )
					throw new HttpException( 'sign does not match', HttpException::Unprocessable_Entity );
				break;
			case self::ACTION_PUSH:
				if( strtolower($data->sign) != $this->signPush( $channel->secret, $data) )
					throw new HttpException( 'sign does not match', HttpException::Unprocessable_Entity );
				break;
		}

		return $channel->id ;
	}

	public function signPush( $secret, $data )
	{
		return sha1( $secret
			. $data->from . $data->from_at
			. $data->to . $data->to_at
			. $data->body
		);
	}

	public function signPull( $secret, $data )
	{
		return sha1( $secret
			. $data->from
			. $data->to
		);
	}

}

class Db
{
	/**
	 * @var PDO $pdo
	 */
	protected $pdo ;

	public function __construct( $dsn, $user, $pass )
	{
		$this->pdo = new PDO( $dsn, $user, $pass,
			[
				PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "UTF8"',
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
			]
		);
		$this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
	}

	public function queryOne( $sql, array $values = [] )
	{
		$sth = $this->pdo->prepare($sql);
		$sth->execute( $values );
		return $sth->fetch();
	}

	public function query( $sql, array $values = [] )
	{
		$sth = $this->pdo->prepare($sql);
		$sth->execute( $values );
		return $sth->fetchAll();
	}

	public function insertOrUpdate( $table, $objects )
	{
		if( ! is_array($objects) )
		{
			$objects = [ $objects ];
		}
		foreach( $objects as $obj )
		{
			$this->processDbTimestamps( $obj );

			$values = [] ;
			if( ! empty($obj->id) )
			{
				$fields = '' ;
				foreach( get_object_vars($obj) as $k => $v )
				{
					if( $k == 'id' )
						continue ;
					if( $fields!='')
						$fields.=', ';
					$fields.='`'.$k.'`=?';
					array_push( $values, $v);
				}
				$sql = 'UPDATE `'.$table.'` SET '.$fields.' WHERE id = '.intval($obj->id) ;
			}
			else
			{
				$fields = '' ; $qmarks='';
				foreach( ((array)$obj) as $k => $v )
				{
					if( $fields!='')
					{
						$fields.=', '; $qmarks.=',';
					}
					$fields.='`'.$k.'`';
					$qmarks.='?';
					array_push($values, $v);
				}
				$sql = 'INSERT INTO `'.$table.'` ('.$fields.') VALUES ('.$qmarks.')' ;
			}

			$sth = $this->pdo->prepare($sql);
			if( $sth->execute( $values ) === false )
				throw RuntimeException('DB exec failed');
		}
	}

	protected function processDbTimestamps( $obj )
	{
		if( array_key_exists('created_at', $obj) )
		{
			if( empty($obj->created_at) )
				$obj->created_at = date('Y-m-d H:i:s');
		}
		if( array_key_exists('updated_at', $obj) )
		{
			$obj->updated_at = date('Y-m-d H:i:s');
		}
	}

}

class HttpException extends Exception
{
	const Forbidden = 403 ;
	const Unprocessable_Entity = 422 ;
	const Internal_Server_Error = 500 ;
}
