
CREATE TABLE `channels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `hash` varchar(8) DEFAULT NULL,
  `secret` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

CREATE TABLE `queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` varchar(45) DEFAULT NULL,
  `sent` tinyint(1) NOT NULL DEFAULT '0',
  `from` varchar(20) DEFAULT NULL,
  `from_at` datetime DEFAULT NULL,
  `to` varchar(20) DEFAULT NULL,
  `to_at` datetime DEFAULT NULL,
  `body` text CHARACTER SET utf8mb4,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `queue_channelidsent` (`channel_id`,`sent`),
  KEY `queue_from` (`from`),
  KEY `queue_to` (`to`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

