# AndSMS2Web

A simple Android SMS to Web and Web to SMS gateway.

Targeted for at least Android 2.3.6 (API 10) to permit the use of old phones (tested with a Samsung Galaxy Y GT-S5369).

It catch all arrived SMS and put then onto the web.

I do this code to work with [WebMessagesDispatcher](https://github.com/Cyrille37/WebMessagesDispatcher) and by extention with [BotQ](https://github.com/TheCitizenCrew/BotQ).

This software is shared under [MIT LICENSE](LICENSE).

## Remote configuration

Configure the Android GW via SMS:
* Enter a code in the App, eg. @AZ123
* Send it a SMS with first line like : `@AZ123`
 * with config stuff following, one per line:
 * url: `url=http://toto.com/gw.php?channel={channel}`
 * new sms to send pull freq. in milliseconds: `pullfreq=2000`
 * channel (auth id): `channel=<a string>`
 * secret (auth pass): `secret=<a string>`

## TODOs

* Storing no pushed (failed) messages for retry later
 * If the sms push into db success, the sms is forgeten
 * If the sms push failed, the sms is stored in the phone for retry later

## Miscellaneous

