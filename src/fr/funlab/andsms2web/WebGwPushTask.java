package fr.funlab.andsms2web;

import java.io.InputStream;
import java.security.MessageDigest;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import fr.funlab.andsms2web.WebGwServiceImpl.FireWorkErrors;
import fr.funlab.andsms2web.WebGwServiceImpl.FireWorkEvents;
import android.os.AsyncTask;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * 
 */
class WebGwPushTask extends AsyncTask<Void, Integer, String>
{
	final String LOG_TAG = this.getClass().getSimpleName();

	/**
	 * FIXME: communiquer autrement avec le service !!
	 */
	WebGwServiceImpl srv ;

    WebGwData webGwData ;
	public SmsMessage sms ;

	public WebGwPushTask( WebGwServiceImpl srv, WebGwData webGwData, SmsMessage sms)
	{
    	Log.d(LOG_TAG, "WebGwPushTask()");

    	this.srv = srv ;
		this.webGwData = webGwData ;
		this.sms = sms ;
    }

    // Surcharge de la méthode doInBackground (Celle qui s'exécute dans une Thread à part)
    @Override
    protected String doInBackground( Void... dummy )
    {
    	Log.d(LOG_TAG, "doInBackground()");
        try
        {
        	Log.d(LOG_TAG, "doInBackground() construct json");

        	JSONObject jsonData = new JSONObject();
			jsonData.put("from", this.sms.getOriginatingAddress() )
			.put("from_at", Long.toString(this.sms.getTimestampMillis()) )
			.put("to", webGwData.phone )
			.put("to_at", Long.toString(System.currentTimeMillis()) )
			.put("body", this.sms.getMessageBody());

			MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update( webGwData.secret.getBytes() );
			md.update( jsonData.getString("from").getBytes() );
			md.update( jsonData.getString("from_at").getBytes() );
			md.update( jsonData.getString("to").getBytes() );
			md.update( jsonData.getString("to_at").getBytes() );
			md.update( jsonData.getString("body").getBytes() );
			byte[] digest = md.digest();
			//jsonData.put( "sign", Base64.encodeToString( digest, Base64.DEFAULT) );
			//jsonData.put( "sign", String.format( "%02X", digest) );
			jsonData.put( "sign", MainActivity.bytesToHex(digest) );

			//JSONObject json = new JSONObject();
			//json.put( "data", jsonData );
			JSONObject json = jsonData ;

			String url = webGwData.url +"?action=push&channel="+webGwData.channel ;

			Log.d(LOG_TAG, "doInBackground() construct http");

			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost( url );
			// passes the results to a string builder/entity
			// important: utf-8 charset
			StringEntity se = new StringEntity(json.toString(), HTTP.UTF_8);
			se.setContentType("application/json");
			httpPost.setEntity(se);
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json; charset=utf-8");
			Log.d(LOG_TAG, "processMessage() httpclient executing url ["+url+"]");

			HttpResponse response = httpclient.execute(httpPost);

			InputStream is = response.getEntity().getContent();
			String content = MainActivity.inputStreamToString(is);

			int statusCode = response.getStatusLine().getStatusCode();

			Log.i(LOG_TAG, "doInBackground() statusCode: "+statusCode+", content: "+content );

			if( statusCode == 200)
			{
				//json =  new JSONObject( content );
				//if( ! json.isNull("dispatched") )
				//	return "OK";
			}
			else
			{
				throw new Exception("Network failed with code " + statusCode + " - content:[" + (content==null?"null":content)+"]");
			}

        }
        catch( Exception e )
        {
        	Log.d(LOG_TAG, "doInBackground() ERROR: "+e.getMessage() );
        	e.printStackTrace();

            return (LOG_TAG + " error: "+e.getMessage() );
        }
        return "OK";
    }

    /**
     * Usage: call publishProgress( x ) in doInBackground()
     * (s'exécute dans la Thread de l'IHM ?)
     * 
     * (non-Javadoc)
     * @see android.os.AsyncTask#onProgressUpdate(java.lang.Object[])
     */
    @Override
    protected void onProgressUpdate( Integer... diff )
    {
    }


    /**
     * (s'exécute dans la Thread de l'IHM ?)
     * (non-Javadoc)
     * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
     */
    @Override
    protected void onPostExecute( String message )
    {
    	Log.d(LOG_TAG, "onPostExecute(): message: "+message );

    	if( message == "OK" )
    	{
    		this.srv.fireWorkEvent(FireWorkEvents.SMS_PUSHED, 1 );
    		return ;
    	}

    	this.srv.fireWorkError( FireWorkErrors.PUSH_ERROR, message );
    }

}
