package fr.funlab.andsms2web;

import android.os.Binder;

public class WebGwServiceBinder extends Binder
{
    private WebGwService service = null;
    
    public WebGwServiceBinder(WebGwService service)
    {
        super();
        this.service = service;
    }
 
    public WebGwService getService(){
        return service;
    } 
}
