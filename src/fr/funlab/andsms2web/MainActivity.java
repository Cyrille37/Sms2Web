package fr.funlab.andsms2web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements WebGwServiceListener
{
	public static final String PREFS_NAME = "AndSms2WebPref";
	public static final String PREF_CONFIGKEY = "configKey";
	public static final String PREF_PHONENUMBER = "phone";

	final String LOG_TAG = this.getClass().getSimpleName();

	SharedPreferences settings ;

	WebGwData webGwData = new WebGwData();

	class Counters
	{
		public int smsReceived = 0 ;
		public int smsPushed = 0 ;
		public int smsPushError = 0 ;

		public int smsPulled = 0 ;
		public int smsSent = 0 ;
		public int smsPullError = 0 ;
	} ;
	Counters counters = new Counters();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		Log.i(this.getClass().getName(), "onCreate()");

		setContentView(R.layout.activity_main);

		webGwData.phone = this.getPhoneNumber();

		this.settings = getSharedPreferences(PREFS_NAME, android.content.Context.MODE_PRIVATE);
		// Load preferences
		this.webGwData.updateFromSettings(this.settings);
		// update UI
		onUpdateConfig( this.webGwData, null );

		final Button buttonConfigKeySet = (Button) findViewById(R.id.buttonConfigKeySet);
		buttonConfigKeySet.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v)
			{
				Log.d(LOG_TAG, "buttonConfigKeySet()");

				EditText editText = (EditText) findViewById(R.id.edittextConfigKeyValue);

				webGwData.configKey = editText.getText().toString() ;

				SharedPreferences.Editor editor = settings.edit();
				editor.putString( PREF_CONFIGKEY, webGwData.configKey );
				editor.commit();
			}
		});

		//
		// Registering the BroadcastReceiver
		//
		Log.d(this.getClass().getName(), "onCreate() Registring the BroadcastReceiver ...");

	    IntentFilter filter = new IntentFilter();
	    filter.addAction("android.provider.Telephony.SMS_RECEIVED");
	    registerReceiver( this.broacastReceiver, filter );

	    /*
	     * In standby ...
	     * 
	    filter = new IntentFilter();
	    filter.addAction("android.provider.Telephony.MMS_RECEIVED");
	    registerReceiver( this.broacastReceiver, filter );

	    filter = new IntentFilter();
	    filter.addAction("android.provider.Telephony.WAP_PUSH_DELIVER");
	    registerReceiver( this.broacastReceiver, filter );

	    filter = new IntentFilter();
	    filter.addAction("android.provider.Telephony.WAP_PUSH_RECEIVED");
	    try {
			filter.addDataType("application/vnd.wap.mms-message");
		    registerReceiver( this.broacastReceiver, filter );
		} catch (MalformedMimeTypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/

	    //
	    // Binding to SmsReceiver to listening to it's events.
	    //
		Log.d(this.getClass().getName(), "onCreate() Binding to SmsReceiver ...");

		Intent intent = new Intent( this,WebGwServiceImpl.class );
		intent.putExtra( WebGwData.INTENT_NAME, webGwData );
	    bindService( intent, this.smsReceiverConnection, Context.BIND_AUTO_CREATE);
	}


	/**
	 * Update the UI from WebGwData
	 * @param webGwData
	 */
	@Override
	public void onUpdateConfig( final WebGwData webGwData, final String fromPhone )
	{
		this.runOnUiThread(new Runnable()
		{
			public void run()
			{
		        Log.d(LOG_TAG, "onUpdateConfig()" );

				MainActivity.this.webGwData = webGwData ;

				EditText et ;
				et = (EditText) findViewById(R.id.edittextConfigKeyValue );
				et.setText( webGwData.configKey );
				//MainActivity.this.settings.edit().putString(WebGwData.PREF_CONFIGKEY, webGwData.configKey).apply();
				
				et = (EditText) findViewById(R.id.editTextChannelValue );
				et.setText( webGwData.channel );
				//MainActivity.this.settings.edit().putString(WebGwData.PREF_CHANNEL, webGwData.channel).apply();

				et = (EditText) findViewById(R.id.editTextUrlValue );
				et.setText( webGwData.url );
				//MainActivity.this.settings.edit().putString(WebGwData.PREF_URL, webGwData.url).apply();

				et = (EditText) findViewById(R.id.editTextPoolFreqValue );
				et.setText( Integer.toString(webGwData.pullFreq) );
				//MainActivity.this.settings.edit().putInt(WebGwData.PREF_PULLFREQ, webGwData.pullFreq).apply();

				CheckBox cb = (CheckBox) findViewById(R.id.checkBoxOnlyFromMe );
				cb.setChecked( webGwData.fromMe );
				//MainActivity.this.settings.edit().putBoolean(WebGwData.PREF_FROMME, webGwData.fromMe).apply();

				MainActivity.this.webGwData.updateSettings( MainActivity.this.settings.edit() );
				
				if( fromPhone != null )
				{
					Log.i(LOG_TAG, "onUpdateConfig() replying a sms..." );
					/* Launch the UI sms ...
					Intent intentSms = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + fromPhone ));     
					intentSms.putExtra("sms_body", "Config set"); 
					startActivity( intentSms );
					*/
					SmsManager sms = SmsManager.getDefault();
				    sms.sendTextMessage( fromPhone, null, "Config received", null, null );
				}

			}
		});		

	}

	@Override
	public void onSmsPulled( int count )
	{
		this.runOnUiThread(new Runnable()
		{
			public void run()
			{
		        Log.d(LOG_TAG, "onSmsReceived()");

		        counters.smsPulled ++ ;
		        TextView tv = (TextView) findViewById(R.id.textviewPulledSmsValue);
		        tv.setText( Integer.toString(counters.smsPulled) );
			}
		});
	}

	@Override
	public void onSmsSent(int count)
	{
		this.runOnUiThread(new Runnable()
		{
			public void run()
			{
		        Log.d(LOG_TAG, "onSmsSent()");

		        counters.smsSent ++ ;
		        TextView tv = (TextView) findViewById(R.id.textviewSentSmsValue);
		        tv.setText( Integer.toString(counters.smsSent) );
			}
		});
	}

	@Override
	public void onSmsPushed( int count )
	{
		this.runOnUiThread(new Runnable()
		{
			public void run()
			{
		        Log.d(LOG_TAG, "onSmsPushed()");

		        counters.smsPushed ++ ;
		        TextView tv = (TextView) findViewById(R.id.textViewPushedSmsValue);
		        tv.setText( Integer.toString(counters.smsPushed) );
			}
		});
	}

	public void onSmsPushError( final String error )
	{
		this.runOnUiThread(new Runnable()
		{
			public void run()
			{
		        Log.d(LOG_TAG, "onSmsPushError() ERROR: "+error );
		        
		        counters.smsPushError ++ ;
		        TextView tv = (TextView) findViewById(R.id.TextViewPushErrorValue);
		        tv.setText( Integer.toString(counters.smsPushError) );
		        
				Toast.makeText(
					MainActivity.this.getApplicationContext(),
					"PUSH ERROR: " + error,
					Toast.LENGTH_LONG
				).show();	
			}
		});
	}

	@Override
	public void onSmsPullError( final String error)
	{
		this.runOnUiThread(new Runnable()
		{
			public void run()
			{
		        Log.d(LOG_TAG, "onSmsPullError() ERROR: "+error );
		        
		        counters.smsPullError ++ ;
		        TextView tv = (TextView) findViewById(R.id.TextViewPullErrorValue);
		        tv.setText( Integer.toString(counters.smsPullError) );
		        
				Toast.makeText(
					MainActivity.this.getApplicationContext(),
					"PULL ERROR: " + error,
					Toast.LENGTH_LONG
				).show();	
			}
		});		
	}

	/**
	 * Bind to SmsReceiverService local service and subscribe to it's events.
	 */
	private final ServiceConnection smsReceiverConnection = new ServiceConnection()
	{
		@Override
		public void onServiceConnected(ComponentName name, IBinder binder)
		{
	        Log.d(LOG_TAG, "SmsReceiverService Connected!");
	        WebGwService service = ((WebGwServiceBinder)binder).getService();
	        service.addListener(MainActivity.this);
		}
		@Override
		public void onServiceDisconnected(ComponentName name)
		{
	        Log.d(LOG_TAG, "SmsReceiverService Disconnected!");
		}  
	};

	/**
	 * Listening for broadcast messages filtered (SMS_RECEIVED) at onCreate() time.
	 */
	private final BroadcastReceiver broacastReceiver = new BroadcastReceiver()
    {
		@Override
		public void onReceive(Context context, Intent intent)
		{
			String action = intent.getAction();
			String type = intent.getType();
			Log.i(LOG_TAG, "onReceive() action: "+action.toString()+", type: "+type );

			if( action.equals("android.provider.Telephony.SMS_RECEIVED") )
			{
				Log.i(LOG_TAG, "onReceive() SMS_RECEIVED");

				MainActivity.this.runOnUiThread(new Runnable()
				{
					public void run()
					{
						MainActivity.this.counters.smsReceived ++ ;

				        TextView tv = (TextView) findViewById(R.id.textViewReceivedSmsValue);
				        tv.setText( Integer.toString(counters.smsReceived) );
					}
				});

				Intent srvIntent = new Intent( MainActivity.this, WebGwServiceImpl.class );
				srvIntent.putExtras( intent.getExtras() );
				srvIntent.putExtra( WebGwData.INTENT_NAME, webGwData );
				startService( srvIntent );

				// FIXME: how to handle service fail and leave sms going to inbox ????
				// TODO: what about several sms to process ? this abortBroadcast is for all sms ...
				// Received SMS will not be put to incoming.
				this.abortBroadcast();
			}
			else if( action.equals("android.provider.Telephony.WAP_PUSH_RECEIVED"))
			{
				// https://maximbogatov.wordpress.com/?s=MMS+in+Android
				// https://maximbogatov.wordpress.com/2011/08/13/mms-in-android/
				// http://stackoverflow.com/questions/3012287/how-to-read-mms-data-in-android
				// https://android.googlesource.com/platform/development/+/master/samples/ApiDemos/src/com/example/android/apis/os
				/*
		           Bundle bundle = intent.getExtras();
		           byte[] buffer ;
		           String str ;
		           
		           int transactionId = bundle.getInt("transactionId");
		           Log.i(LOG_TAG, "onReceive() MMS transactionId: " + transactionId );
		           int pduType = bundle.getInt("pduType");
		           Log.i(LOG_TAG, "onReceive() MMS pduType: " + pduType );
 
		           buffer = bundle.getByteArray("header");
	               str = new String(buffer);
	               Log.i(LOG_TAG, "onReceive() MMS header: " + str );

	               buffer = bundle.getByteArray("data");
	               str = new String(buffer);
	               Log.i(LOG_TAG, "onReceive() MMS data: " + str );
	               
	               Uri uri = Uri.parse("content://mms/");
	               //String selection = "_id = " + mmsId;
	               Cursor cursor = getContentResolver().query(uri, null, null, null, null);
	               if (cursor.moveToFirst()) {
	            	   Log.i(LOG_TAG, "onReceive() Content cursor.moveToFirst()" );
	            	   for( String n : cursor.getColumnNames() )
	            	   {
		            	   Log.i(LOG_TAG, "onReceive() Content ColumnNames: "+n );
	            	   }
	            	   do {
		            	   str = cursor.getString(cursor.getColumnIndex("ct_t"));
			               Log.i(LOG_TAG, "onReceive() ct_t: " + str );
		            	   str = cursor.getString(cursor.getColumnIndex("ct_l"));
			               Log.i(LOG_TAG, "onReceive() ct_l: " + str );
	            	   }  while (cursor.moveToNext());
	               }
				*/
			}
		}
	};

	protected void onDestroy()
	{
		unregisterReceiver(broacastReceiver);
	}

	String getPhoneNumber()
	{
		String phoneNumber;
		TelephonyManager tMgr =
			(TelephonyManager) this.getSystemService(MainActivity.TELEPHONY_SERVICE);
		phoneNumber = tMgr.getLine1Number();
		if (phoneNumber == null) {
			// String getSimSerialNumber = telemamanger.getSimSerialNumber();
		}
		return phoneNumber;
	}

	public static String inputStreamToString(InputStream inputStream)
			throws IOException
	{
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line;
		StringBuilder result = new StringBuilder();
		while ((line = bufferedReader.readLine()) != null)
			result.append(line);
		inputStream.close();
		return result.toString();

	}

	public final static char[] hexArray = "0123456789ABCDEF".toCharArray();
	public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}

}
