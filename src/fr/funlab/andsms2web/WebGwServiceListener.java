package fr.funlab.andsms2web;

public interface WebGwServiceListener
{
	public void onSmsPulled( int count );
	public void onSmsPullError( String error );

	public void onSmsPushed( int count );
	public void onSmsSent( int count );
	public void onSmsPushError( String error );
	
	public void onUpdateConfig( WebGwData webGwData, String fromPhone );
}
