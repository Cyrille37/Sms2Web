package fr.funlab.andsms2web;

import java.io.InputStream;
import java.io.Serializable;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

public class WebGwServiceImpl extends Service implements WebGwService
{
	static final String LOG_TAG = WebGwServiceImpl.class.getSimpleName();
	static final String SMS_EXTRA_NAME = "pdus";

	private WebGwServiceBinder binder ;

	Timer timer ;

	WebGwData webGwData ;

	@Override
	public void onCreate()
	{
	    super.onCreate();
	    Log.d(LOG_TAG, "onCreate()");

	    this.binder = new WebGwServiceBinder( this );

	}

	/**
	 * The service can be called without sms,
	 * just to update this.webGwData.
	 * 
	 * (non-Javadoc)
	 * @see android.app.Service#onStartCommand(android.content.Intent, int, int)
	 */
	@Override
	public int onStartCommand( final Intent intent, int flags, int startId )
	{
	    Log.d(LOG_TAG, "onStartCommand()");

		try
		{
		    this.webGwData = (WebGwData) intent.getSerializableExtra( WebGwData.INTENT_NAME ) ;

		    if( ! webGwData.isValidForHttpTalk() )
		    {
		    	fireWorkError(FireWorkErrors.PUSH_ERROR, "Invalid WebGwData");
		    	return START_NOT_STICKY ;
		    }

		    // Get the SMS map from Intent
			// The Bundle object is a simple map. It contains pairs of keys and values.
			// SMS are placed in this bundle.
			// The key of SMS is SMS_EXTRA_NAME.
			Bundle extras = intent.getExtras();
			// Get received SMS array
		    Object tmp = extras.get( WebGwServiceImpl.SMS_EXTRA_NAME );
		    if( tmp == null )
		    {
		    	// No sms, we leave.
		    	return START_NOT_STICKY ;
		    }

			Object[] smsExtra = (Object[]) tmp ;
			Log.d(LOG_TAG, "onStartCommand() sms count: " + smsExtra.length );

			for( int i = 0; i < smsExtra.length; ++i )
			{
				SmsMessage sms = SmsMessage.createFromPdu((byte[]) smsExtra[i]);

				if( webGwData.updateFromString( sms.getMessageBody() ) )
				{
					Log.i(LOG_TAG,"onStartCommand() webGwData changed");
					fireUpdateConfig( webGwData, sms.getOriginatingAddress() );

					if( this.pullFreq != webGwData.pullFreq )
					{
						if( this.timer != null )
							this.timer.cancel();
						this.timer = new Timer();
						this.pullFreq = webGwData.pullFreq ;
					    this.timer.schedule( new WebGwPullTimerTask(), 2000, this.pullFreq );

					}
				}

				Log.i(LOG_TAG,"onStartCommand() launch WebGwPushTask");

				WebGwPushTask wgpt = new WebGwPushTask( this, webGwData, sms );
				wgpt.execute();
			}
		    
		}
		catch( Exception e )
		{
			Log.e(LOG_TAG, "ERROR onReceive(): " + e.getMessage());
			e.printStackTrace();
			fireWorkError( FireWorkErrors.PUSH_ERROR, LOG_TAG+".onStartCommand() ERROR: "+e.getMessage());
		}

	    return START_NOT_STICKY;
	}

	int pullFreq ;
	
	@Override
	public IBinder onBind(Intent intent)
	{
		Log.d(LOG_TAG, "onBind() " + intent.toString() );
		try
		{
		    this.webGwData = (WebGwData) intent.getSerializableExtra( WebGwData.INTENT_NAME ) ;
		    this.pullFreq = webGwData.pullFreq ;

		    if( this.timer == null )
		    {
				Log.d(LOG_TAG, "onBind() starting Timer..." );
			    this.timer = new Timer();
			    this.timer.schedule( new WebGwPullTimerTask(), 2000, this.pullFreq );
		    }

		}
		catch( Exception e )
		{
			Log.e(LOG_TAG, "ERROR onBind(): " + e.getMessage());
			// MainActivity is not yet registred. @see MainActivity.smsReceiverConnection
			// fireOnSmsReceiverError( "onReceive(): " + e.getMessage() );
		}

		return this.binder;
	}

	@Override
	public void onDestroy()
	{
	    Log.d(LOG_TAG, "onDestroy()");

	    listeners.clear();
	}

	/**
	 * TODO: do that in another Thread
	 * TODO: this method directly firing events ... that's Not a well fashion.
	 * I wanted to launch an AsyncTask from the TimerTask but it's not permitted :-[
	 */
	class WebGwPullTimerTask extends TimerTask
	{
		final String LOG_TAG = this.getClass().getSimpleName();

		boolean pullDone = true ;
		
		@Override
		public void run()
		{
			Log.d(LOG_TAG, "run() pullFreq="+pullFreq);
			try
			{
				if( ! pullDone )
				{
					Log.d(LOG_TAG, "run() Pull running, comeback later...");
					return ;
				}
				pullDone = false ;

				if( pullFreq != webGwData.pullFreq )
				{
					if( timer != null )
						timer.cancel();
					timer = new Timer();
					pullFreq = webGwData.pullFreq ;
				    timer.schedule( new WebGwPullTimerTask(), pullFreq, pullFreq );
				}

				Log.d(LOG_TAG, "run() Pulling channel["+webGwData.channel+"] for ["+webGwData.phone+"] ...");
			    DefaultHttpClient httpclient = new DefaultHttpClient();

			    String url = webGwData.url+"?action=pull&channel="+webGwData.channel ;
				HttpPost httpPost = new HttpPost( url );

				JSONObject json = new JSONObject();
				json.put("from", webGwData.fromMe ? webGwData.phone : "" );
				json.put("to", "");

				MessageDigest md = MessageDigest.getInstance("SHA-1");
				md.update( webGwData.secret.getBytes() );
				md.update( json.getString("from").getBytes() );
				md.update( json.getString("to").getBytes() );
				byte[] digest = md.digest();
				json.put( "sign", MainActivity.bytesToHex(digest) );

				StringEntity se = new StringEntity(json.toString(), HTTP.UTF_8);
				se.setContentType("application/json");
				httpPost.setEntity(se);
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json; charset=utf-8");

				Log.d(LOG_TAG, "run() httpclient.execute...");
				HttpResponse response= httpclient.execute( httpPost );

				Log.d(LOG_TAG, "run() httpclient reponse processing...");
				if( response.getStatusLine().getStatusCode() != 200)
				{
					throw new Exception( "HTTP Failed: "+response.getStatusLine().getStatusCode()+", "+response.getStatusLine().getReasonPhrase() );
				}
				InputStream is = response.getEntity().getContent();
				String content = MainActivity.inputStreamToString(is);
				if (content == null)
				{
					throw new Exception( "HTTP Response is null" );
				}

				/*
				Content: {"sms":{"id":101,"from":"15555215554","to":"+33612121212","body":"#Pong ","sent":0,"created_at":"2016-04-04 16:02:26","updated_at":"2016-04-04 15:17:40"}}
				*/
				Log.i(LOG_TAG,"Content: " + content );

				json =  new JSONObject(content );
				if( ! json.isNull("sms") )
				{
					fireWorkEvent(FireWorkEvents.SMS_PULLED, 1 );

					JSONObject smsJson = json.getJSONObject("sms");
					String to = smsJson.getString("to");
					String body = smsJson.getString("body");

					SmsManager smsManager = SmsManager.getDefault();
					smsManager.sendTextMessage( to, null, body, null, null);

					fireWorkEvent(FireWorkEvents.SMS_SENT, 1 );
					//displayNotification();
				}
				else
				{
					Log.d( LOG_TAG, "no sms, will try again later");
				}

			}
			catch( Exception e )
			{
				Log.e(LOG_TAG, "run() Error: "+e.getMessage());
				e.printStackTrace();
				fireWorkError( FireWorkErrors.PULL_ERROR, LOG_TAG+".run() ERROR: "+e.getMessage() );
			}
			finally
			{
				this.pullDone = true ;
			}
		}	
	}

	private List<WebGwServiceListener> listeners = null;

	@Override
	public void addListener(WebGwServiceListener listener)
	{
		Log.d(LOG_TAG, "addListener()");
		if( listeners == null )
		{
	        listeners = new ArrayList<WebGwServiceListener>();
	    }
	    listeners.add(listener);		
	}

	@Override
	public void removeListener(WebGwServiceListener listener)
	{
		Log.d(LOG_TAG, "removeListener()");
		if( listeners != null )
		{
			listeners.remove(listener);
		}
	}

	protected enum FireWorkEvents
	{
		SMS_PULLED,
		SMS_SENT,
		SMS_PUSHED
	};

	/**
	 * Notification des listeners
	 * @param event
	 * @param count
	 */
	protected void fireWorkEvent( FireWorkEvents event, int count )
	{
	    if( listeners == null )
	    	return ;
	    switch( event )
	    {
	    case SMS_PULLED:
	        for(WebGwServiceListener listener: listeners)
	            listener.onSmsPulled( count );
	    	break;
	    case SMS_SENT:
	        for(WebGwServiceListener listener: listeners)
	            listener.onSmsSent( count );
	    	break;
	    case SMS_PUSHED:
	        for(WebGwServiceListener listener: listeners)
	            listener.onSmsPushed( count );
	    	break;
	    }
	}

	protected void fireUpdateConfig( WebGwData webGwData, String fromPhone )
	{
	    if( listeners == null )
	    	return ;
        for(WebGwServiceListener listener: listeners)
            listener.onUpdateConfig( webGwData, fromPhone);
	}

	enum FireWorkErrors
	{
		PULL_ERROR,
		PUSH_ERROR
	};

	protected void fireWorkError( FireWorkErrors event, String error )
	{
	    if( listeners == null )
	    	return ;
	    switch( event )
	    {
	    case PULL_ERROR:
	        for(WebGwServiceListener listener: listeners)
	            listener.onSmsPullError( error );
	    	break;
	    case PUSH_ERROR:
	        for(WebGwServiceListener listener: listeners)
	            listener.onSmsPushError( error );
	    	break;
	    }
	}

}
