package fr.funlab.andsms2web;

import java.io.Serializable;
import java.util.Locale;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;


public class WebGwData implements Serializable
{
	static final String LOG_TAG = WebGwData.class.getSimpleName();

	private static final long serialVersionUID = 1L;
	public static final String INTENT_NAME = "webGwData" ;

	public static final String PREF_CONFIGKEY = "configKey";
	public static final String PREF_URL = "url";
	public static final String PREF_CHANNEL = "channel";
	public static final String PREF_SECRET = "secret";
	public static final String PREF_FROMME = "fromme";
	public static final String PREF_PULLFREQ = "pullfreq";

	public String configKey, phone, url, channel, secret ;
	/**
	 * Pull only sms "from" me.
	 * If false it will pull ALL not sent sms from the queue.
	 */
	boolean fromMe = true ;
	/**
	 * milliseconds
	 */
	int pullFreq ;

	public WebGwData()
	{
	}

	public WebGwData( String configKey,  String phone, int pullFreq, boolean fromMe , String url, String channel, String secret)
	{
		this.configKey = configKey ;
		this.pullFreq = pullFreq ;
		this.fromMe = fromMe ;
		this.url = url ;
		this.channel = channel ;
		this.secret = secret ;

		this.phone = phone ;
    }

	public boolean isValidForHttpTalk()
	{
		if( this.phone==null || this.phone.isEmpty() )
			return false ;
		if( this.url==null || this.url.isEmpty() )
			return false ;
		if( this.channel==null || this.channel.isEmpty() )
			return false ;
		if( this.secret==null || this.secret.isEmpty() )
			return false ;
		return true ;
	}

	public boolean updateFromString( String configStr )
	{
		Log.d(LOG_TAG,"updateFromString()");

		if( this.configKey == null || this.configKey.trim().equals("") )
			return false ;

		final String eol = System.getProperty("line.separator") ;

		configStr = configStr.trim();

		if( ! configStr.startsWith( this.configKey ))
			return false ;

		Log.d(LOG_TAG,"updateFromString() found configKey:"+this.configKey);

		//
		// A "Ping" command
		// Just the config key with nothing else
		//
		if( configStr.equals( this.configKey ))
		{
			Log.i(LOG_TAG,"updateFromString() Config Ping");
			return true ;
		}

		String[] lines = configStr.split( eol );

		boolean hasChanges = false ;
		for( int i=1; i<lines.length; i++)
		{
			//Log.d(LOG_TAG, "\t> "+lines[i]);
			int pos = lines[i].indexOf("=");
			String k = lines[i].substring(0, pos).trim().toLowerCase(Locale.getDefault());
			String v = lines[i].substring(pos+1).trim().toLowerCase(Locale.getDefault());
			Log.d(LOG_TAG, "\t> kv:["+k+"]["+v+"]");

			if( k.equals("url"))
			{
				this.url = v ; hasChanges = true ;
				Log.i(LOG_TAG,"updateFromString() Config Url: "+this.url);
			}
			else if( k.equals("pullfreq"))
			{
				try{
					this.pullFreq = Integer.valueOf( v ) ; hasChanges = true ;
					Log.i(LOG_TAG,"updateFromString() Config PullFreq: "+this.pullFreq);
				}catch(Exception e)
				{
					Log.e(LOG_TAG,"updateFromString() Config PullFreq ERROR v:["+ v+"]");
				}
			}
			else if( k.equals("fromme"))
			{
				this.fromMe = v == "1" ? true : false ; hasChanges = true ;
				Log.i(LOG_TAG,"updateFromString() Config FromMe: "+this.fromMe);
			}
			else if( k.equals("channel"))
			{
				this.channel = v ; hasChanges = true ;
				Log.i(LOG_TAG,"updateFromString() Config channel: "+this.channel );
			}
			else if( k.equals("secret"))
			{
				this.secret = v ; hasChanges = true ;
				Log.i(LOG_TAG,"updateFromString() Config secret: "+this.secret );
			}
		}

		return hasChanges ;
	}
	
	public void updateFromSettings( SharedPreferences settings )
	{
		/*
		public static final String PREF_FROMME = "fromme";
		public static final String PREF_PULLFREQ = "pullfreq";
		*/

		this.configKey = settings.getString(PREF_CONFIGKEY, "@789");
		this.channel = settings.getString( PREF_CHANNEL, "EE661A2D");
		this.url = settings.getString(PREF_URL, "http://quaipaulbert.giquello.fr/smsgw/gw.php");
		this.secret = settings.getString(PREF_SECRET, "FC18CF533AF96071890830D4A8AD7557");
		this.fromMe = settings.getBoolean(PREF_FROMME, true);
		this.pullFreq = settings.getInt(PREF_PULLFREQ, 3000 );

	}

	public void updateSettings(Editor edit) {

		edit.putString(WebGwData.PREF_CONFIGKEY, configKey);
		edit.putString(WebGwData.PREF_CHANNEL, channel);
		edit.putString(WebGwData.PREF_URL, url);
		edit.putInt(WebGwData.PREF_PULLFREQ, pullFreq);
		edit.putBoolean(PREF_FROMME, fromMe);

		edit.apply();
	}


}
