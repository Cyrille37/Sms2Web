package fr.funlab.andsms2web;

public interface WebGwService
{
	public void addListener(WebGwServiceListener listener);
	public void removeListener(WebGwServiceListener listener);
}
